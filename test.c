#include <stdio.h>
#include <math.h>
struct pairs
    {
        int x,y;
    };
int inputn()
{
    int n;
    printf("enter how many numbers\n");
    scanf("%d",&n);
    return n;
}
int input(int n,int num[n])
{
    int high=0;
    for(int i=0;i<n;i++)
    {
        scanf("%d",&num[i]);
        if(high<num[i])
            high=num[i];
    }
    return high;
}
int divisors(int a, struct pairs b[])
{
    int j=0;
    for(int i=1;i<=sqrt(a);i++)
    {

        if(a%i==0)
        {
           b[j].x=i;
           b[j].y=a/i;
           j++;
        }
    }
    return j;
}
void output(int a,struct pairs b[],int j)
{
    printf("\n%d: ",a);
    for(int i=0;i<j;i++)
        printf("(%d,%d)  ",b[i].x,b[i].y);
}
int main()
{
    int n=inputn();
    int num[n];
    int high=input(n,num);
    struct pairs b[high];
    int j;
    for(int i=0;i<n;i++)
    {
        j=divisors(num[i],b);
        output(num[i],b,j);
    }
    return 0;
}